import firebase from '../../config/fbConfig'

export const createFilm = (film) => {
    return (dispatch, getState, { getFirestore}) => {
        const firestore = getFirestore();
        const profile = getState().firebase.profile;
        const authorId = getState().firebase.auth.uid;
        //make async call to database
        firestore.collection('films').add({
            ...film,
            userFirstName: profile.firstName,
            userLastName: profile.lastName,
            authorId: authorId,
            createdAt: new Date()
        }).then(() => {
            dispatch({type: 'CREATE_FILM_SUCCESS'}, film);

        }).catch((err) => {
            dispatch({ type: 'CREATE_FILM_ERROR', err});

        })
    }
}