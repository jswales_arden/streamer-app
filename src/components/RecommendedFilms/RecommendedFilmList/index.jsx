import React from 'react';
import RecommendedFilmItem from '../RecommendedFilmItem';

function RecommendedFilmList(props) {
  const headingStyle = {
    marginLeft: '6.5rem',
    marginTop: '2rem'
  };
  
  const tableStyle = {
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: '2rem',
    width: '90%',
    border: '5px solid pink'
  };
  
  const films = props.films;
  
  const renderRows = films.map((film, i) =>
    <RecommendedFilmItem key={i} film={film} onFilmSelectedClicked={() => props.onFilmSelectedClicked(i)} />
  );    

  return (
    <React.Fragment>
      <h4 style={headingStyle}>Available films</h4>
      <table class="highlight">
        <thead>
          <tr>
            <th>Name</th>
            <th>Genre</th>
            <th />
          </tr>
        </thead>
        <tbody>
          { renderRows }
        </tbody>
      </table>
      <center><a class="waves-effect waves-light btn">Refresh</a></center>

    </React.Fragment>
  );
}

export default RecommendedFilmList;