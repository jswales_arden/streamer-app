import React, {Component} from 'react';
import firebase from '../../config/fbConfig'
import RecommendedFilmList from './RecommendedFilmList';

var db = firebase.firestore(); 

class RecommendedFilms extends Component {
    constructor(props) {
        super(props);
        this.state = {
            films: []
        };
        this.handleFilmSelectedClicked = this.handleFilmSelectedClicked.bind(this);
    }

    findFilmByKey(films, id) {
        var i;
        for (i = 0; i < films.length; ++i) {
            if (films[i].key === id) {
                return films[i];
            }
        }
        return null;
    }

    findFilmByFilmID(films, id) {
        var i;
        for (i = 0; i < films.length; ++i) {
            if (films[i].filmID === id) {
                return films[i];
            }
        }
        return null;
    }
  
    componentWillMount() {
        var userID = firebase.auth().currentUser.uid;
        const films = [];
        const recentFilms = [];
        const recommendedFilms = [];

        db.collection("films")
            .get().then((filmQuery) => {
                filmQuery.forEach(doc => {
                    const film = {
                        key: doc.id,
                        createdAt: doc.data().createdAt,
                        filmTitle: doc.data().filmTitle,
                        filmGenre: doc.data().filmGenre
                    }
                    films.push(film);
                });

                db.collection("userfilm")
                    .get().then((querySnapshot) => {
                        querySnapshot.forEach((doc) => {
                            console.log(doc.data().userID.id);
                            console.log(doc.data().filmID.id);
                            console.log(doc.data().liked);
                            if (doc.data().userID.id === userID) {
                                const recentFilm = {
                                    id: doc.id,
                                    filmID: doc.data().filmID.id,
                                    liked: doc.data().liked
                                }
                                const film = this.findFilmByKey(films, recentFilm.filmID)
                                if (film) {
                                    recentFilm.filmGenre = film.filmGenre;  
                                }
                                recentFilms.push(recentFilm);   
                            }
                        });

                        var genreList = []

                        var i;
                        for (i = 0; i < recentFilms.length; ++i) {
                            genreList.push(recentFilms[i].filmGenre)
                        }

                        if (genreList.length > 0) {
                            for (i = 0; i < films.length; ++i) {
                                if (films[i].filmGenre === genreList[0]) {
                                    const film = this.findFilmByFilmID(recentFilms, films[i].key)
                                    if (film == null) {
                                        films[i].selected = false;
                                        recommendedFilms.push(films[i])
                                        if (recommendedFilms.length === 5) {
                                            break;
                                        }
                                    }
                                }
                            }
                        }

                        if (recommendedFilms.length < 5) {
                            for (i = 0; i < films.length; ++i) {
                                const film = this.findFilmByFilmID(recentFilms, films[i].key)
                                if (film == null) {
                                    films[i].selected = false;
                                    recommendedFilms.push(films[i])
                                    if (recommendedFilms.length === 5) {
                                        break;
                                    }
                                }
                            }
                        }
                        
                        this.setState({films: recommendedFilms});
                })
            })
    }

    handleFilmSelectedClicked(index) {
        var userID = firebase.auth().currentUser.uid;
        const {films} = this.state;
        films[index].selected = true;

        return db.collection("userfilm").doc().set(
            {
                userID: db.doc('users/' + userID),
                filmID: db.doc('films/' + films[index].key),
                liked: false
            }
        )
        .then(() => {
             this.setState({films});
        })
    }

    render(){
        const {films} = this.state;
       
        return(
            <RecommendedFilmList films={films} onFilmSelectedClicked={this.handleFilmSelectedClicked} />
        )
    }
}


function enableRefresh() {
    
}
export default RecommendedFilms;

