import React from 'react';

function RecommendedFilmItem(props) { 
  const film = props.film;
    
  return (
    <tr>
        <td>{film.filmTitle}</td>
        <td>{film.filmGenre}</td>
        <td>
          {<a class="waves-effect waves-light btn" disabled={film.selected === true} onClick={props.onFilmSelectedClicked}>Select</a>}
        </td>
    </tr>
  )
}

export default RecommendedFilmItem;