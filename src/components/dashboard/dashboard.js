import React, { Component } from 'react';
import Notifications from "./notifications";
import FilmList from '../films/filmList';
import { connect } from 'react-redux'
import { firestoreConnect } from  'react-redux-firebase'
import { compose } from 'redux'
import {Redirect} from 'react-router-dom'
import moment from 'moment';

class Dashboard extends Component {
    render(){
        const {films, auth, notifications} = this.props;
        if (!auth.uid) return <Redirect to = '/login' />

        return(
            <div className="dashboard container">
                        <h5>Welcome to Streamer</h5>

                <div className="row">
                    <div className="col s12 m6">
                        <FilmList  films = {films}/>
                    </div>
                    <div className="col s12 m5 offset-m1">
                        <Notifications notifications = {notifications}/>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        // films: state.film.films
        films: state.firestore.data.films,
        auth: state.firebase.auth,
        notifications: state.firestore.ordered.notifications
    }
}
export default compose(
    firestoreConnect([
        { collection: 'films' , limit: 5 },
        { collection: 'notifications', limit: 3, orderBy: ['time', 'desc']}
    ]),
    connect(mapStateToProps)
)(Dashboard);
//export default (Dashboard);