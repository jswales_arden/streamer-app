import React, {Component} from 'react';
import { Redirect } from 'react-router-dom'
import {signUp} from '../../store/actions/authActions'
import {connect} from 'react-redux';
class SignUp extends Component {
    state = {
        email: '',
        password: '',
        firstName: '',
        lastName:'',
        bio: ''
    }
    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    }
    handleSubmit = (e) => {
       e.preventDefault(); //prevents default refresh of the page after form is submitted
       this.props.signUp(this.state)
    }
    render() {
        const {auth, authError} = this.props;
        if (auth.uid) return <Redirect to='/' /> 
        return (
            <div className="container">
            <form onSubmit={this.handleSubmit} className="white">
                <h5 className="grey-text-text-darken-3">Sign up</h5>
                <p>To sign up for an account, please fill out the following details.</p>
                    <div className="input-field">
                        <label htmlFor="email">Email</label>
                        <input type="email" id="email" onChange={this.handleChange}/>
                    </div>
                    <div className="input-field">
                        <label htmlFor="firstName">First name</label>
                        <input type="text" id="firstName" onChange={this.handleChange}/>
                    </div>
                    <div className="input-field">
                        <label htmlFor="lastName">Last name</label>
                        <input type="text" id="lastName" onChange={this.handleChange}/>
                    </div>
                    <div className="input-field">
                        <label htmlFor="bio">One line bio</label>
                        <input type="text" id="bio" onChange={this.handleChange}/>
                    </div>
                    <div className="input-field">
                        <label htmlFor="password">Password</label>
                        <input type="password" id="password" onChange={this.handleChange}/>
                    </div>
                    <div className = "input-field">
                        <button className="btn pink lighten-1 z-depth-0">Sign up</button>
                    </div>
                    <div className="red-text center">
                        {authError? <p>{authError}</p>: null}
                    </div>

            </form>
                
            </div>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        auth: state.firebase.auth,
        authError: state.auth.authError
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        signUp: (newUser) => dispatch(signUp(newUser))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(SignUp);