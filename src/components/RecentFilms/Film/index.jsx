import React, {Component} from 'react';
import PropTypes from 'prop-types';

function Film(props) { 
  const film = props.film;
    
  return (
    <tr>
        <td>{film.filmTitle}</td>
        <td>{film.filmGenre}</td>
        <td>{film.liked ? 'yes' : 'no'}</td>
        <td>
          {<a class="waves-effect waves-light btn" disabled={film.liked === true} onClick={props.onFilmLikeClicked}>Like</a>}
        </td>
    </tr>
  )
}

// Film.propTypes = {
//     film: PropTypes.shape(
//         {
//             name: PropTypes.string,
//             genre: PropTypes.string
//         }
//     ).isRequired
// }

export default Film;