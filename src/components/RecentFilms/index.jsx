import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Film from './Film';
import firebase from 'firebase';

function RecentFilms(props) {
  const headingStyle = {
    marginLeft: '6.5rem',
    marginTop: '2rem'
  };
  
  const tableStyle = {
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: '2rem',
    width: '90%',
    border: '5px solid pink'
  };
  
  const films = props.films;
  
  const renderRows = films.map((film, i) =>
    <Film key={i} film={film} onFilmLikeClicked={() => props.onFilmLikeClicked(i)} />
  );    



  return (
    
    <React.Fragment>
      <h4 style={headingStyle}>Recent films</h4>
      <table class="highlight">
        <thead>
          <tr>
            <th>Name</th>
            <th>Genre</th>
            <th>Liked</th>
            <th />
          </tr>
        </thead>
        <tbody>
          { renderRows }
        </tbody>
      </table>
      <center><a class="waves-effect waves-light btn">Refresh</a></center>

    </React.Fragment>
  );
}

// Film.propTypes = {
//   films: PropTypes.array(PropTypes.shape(
//       {
//           name: PropTypes.string,
//           genre: PropTypes.string
//       }
//   ))
// }

export default RecentFilms;