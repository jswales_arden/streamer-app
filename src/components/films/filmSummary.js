import React from 'react'
import moment from 'moment'
//import filmReducer from '../../store/reducers/filmReducer';
const FilmSummary = ({film}) => {
    return (
        <div className="card z-depth-0 film-summary">
            <div className="card-content grey-text text-darken-3">
                <span className="card-title "> {film.filmTitle} </span>
                <p>Watched by {film.userFirstName} {film.userLastName}</p>
                <p className="grey-text">{moment(film.createdAt.toDate()).calendar()}</p>
            </div>
        </div>
    )
}

export default FilmSummary;