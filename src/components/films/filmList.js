import React from 'react';
import FilmSummary from './filmSummary';
import { Link } from 'react-router-dom'
import { fileURLToPath } from 'url';

/* const FilmList = ({films}) =>
{
    return (
        <div className="film-list section">
            { films && Object.values(films).map((film, key) => {
                return (
                    <FilmSummary film={film} key={key} />
                )
            })}

        </div>
    )
} */


const FilmList = ({films}) =>
{
    return (
        <div className="film-list section">
        { films && Object.values(films).map(film => {
          return (
            // <Link to={'/film/' + film.id} key={film.id}>
               <FilmSummary key={film.id} film={film} />
            // </Link>
          )
        })}  
      </div>
    )
}

const personalFilmList = ({films}) =>
{
  return (
    <div className="film-list section">
    { films && Object.values(films).map(film => {
      return (
        // <Link to={'/film/' + film.id} key={film.id}>
           <FilmSummary film={film} />

           
        // </Link>
      )
    })}  
  </div>
)
}



export default FilmList;