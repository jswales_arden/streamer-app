import React, {Component} from 'react';
import {connect} from 'react-redux';
import {createFilm} from '../../store/actions/filmActions';
import {Redirect} from 'react-router-dom'
class addFilm extends Component {
    state = {
        filmTitle: '',
        filmGenre: '',
        filmSynopsis:'',
        authorId: '',
        youtube: '',
        createdAt: ''
    }
    handleChange = (e) => {

        this.setState({
            [e.target.id]: e.target.value
        })
    }
    handleSubmit = (e) => {
       e.preventDefault(); //prevents default refresh of the page after form is submitted
       this.props.createFilm(this.state);
       this.props.history.push('/');
    }
    render() {
        const { auth } = this.props;
        if (!auth.uid) return <Redirect to='/login' />


        return (
            <div className="container">
            <form onSubmit={this.handleSubmit} className="white">

                <h5 className="grey-text-text-darken-3">Watched a new film?</h5>
                <p>Record it here....</p>

                    <div className="input-field">
                        <label htmlFor="filmTitle">Film title</label>
                        <input type="text" id="filmTitle" onChange={this.handleChange}/>
                    </div>

                    <div className="input-field">
                        <label htmlFor="filmGenre">Film genre</label>
                        <input  type="text" id="filmGenre"  onChange={this.handleChange}></input>
                    </div>

                    <div className="input-field">
                        <label htmlFor="filmSynopsis">Film synopsis</label>
                        <textarea name="text-area" id="filmSynopsis" className="materialize-textarea" onChange={this.handleChange}></textarea>
                    </div>
                    <div className="input-field">
                        <label htmlFor="youtube">YouTube trailer link</label>
                        <input  type="text" id="youtube"  onChange={this.handleChange}></input>
                    </div>

                    

                    <div className = "input-field">
                        <button className="btn pink lighten-1 z-depth-0">Add film</button>
                    </div>

            </form>
                
            </div>
            
        )
    }
}

const mapStateToProps = (state) => {
    return {
        auth: state.firebase.auth
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        createFilm: (film) => dispatch(createFilm(film))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(addFilm);