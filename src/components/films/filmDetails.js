import React from 'react'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import { compose } from 'redux'
import { Redirect } from 'react-router-dom'
import moment from 'moment'

const FilmDetails = (props) => {
    const {film, auth} = props;
    if (!auth.uid) return <Redirect to ='/login'/>

    if (film) {
        return (
        <div className="container section film-details">
            <div className="card z-depth-0">
            <div className="card-content">
                <span className="card-title">{film.title}</span>
                <p>{film.content}</p>s
            </div>
            <div className="card-action grey lighten-4 grey-text">
                <div>Posted by {film.userFirstName} {film.userLastName}</div>
                <div>{moment(film.createdAt.toDate()).calendar()}</div>
            </div>
            </div>
        </div>
        )
    }
}
const mapStateToProps = (state, ownProps) => {
    // console.log(state);
    const id = ownProps.match.params.id;
    const films = state.firestore.data.films;
    const film = films ? film[id] : null
    return {
      film: film,
      auth: state.firebase.auth
    }
  }
  
  export default compose(
    connect(mapStateToProps),
    firestoreConnect([{
      collection: 'films'
    }])

  )(FilmDetails)