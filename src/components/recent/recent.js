//imports
import React, {Component} from 'react';
import firebase from '../../config/fbConfig'
import RecentFilms from '../RecentFilms';

//GET USER INFORMATION FROM FIREBASE AUTH
//var user = firebase.auth().currentUser;
//var name, email, photoUrl, uid, emailVerified;

//FIREBASE DATABASE CONNECTION
var db = firebase.firestore(); 
//END FIREBASE CONNECTION


firebase.auth().onAuthStateChanged((user) => {
    if (user) {
      // User logged in already or has just logged in.
      const userID = user.uid;
    } else {
      // User not logged in or has just logged out.
    }
  });
//FILM DISPLAY CONSTRUCTOR
class recent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            films: []
        };
        this.handleFilmLikeClicked = this.handleFilmLikeClicked.bind(this);
      }
    componentWillMount() {
        // const {films} = this.state;
        var userID = firebase.auth().currentUser.uid;

        const films = [];
        const recentFilms = [];
        //console.log("Current user "+user.uid);
        //if (!auth.uid) return <Redirect to = '/login' />
        // const test = db.collection("users").where("id", "==", "3kZwctBwV1UGAkNJ97BX");

        // const userFilms = db.collection("userfilm").where("userID", "==", "3kZwctBwV1UGAkNJ97BX");
        // const watchFilm = user.collection("watchedFilms").doc("DLbcBn5epeZPlibd44pQ");
        // const test3 = db.collection("watchedFilms").doc("DLbcBn5epeZPlibd44pQ");
        // var here = 'Hello';
        db.collection("userfilm")
        .get().then((querySnapshot) => {
                querySnapshot.forEach((doc) => {
                    console.log(doc.data().userID.id);
                    console.log(doc.data().filmID.id);
                    console.log(doc.data().liked);
                    if (doc.data().userID.id === userID) {
                        const recentFilm = {
                            id: doc.id,
                            filmID: doc.data().filmID.id,
                            liked: doc.data().liked
                        }
                        // const film = {
                        //     createdAt: doc.data().createdAt,
                        //     filmTitle: doc.data().filmTitle,
                        //     filmGenre: doc.data().filmGenre,
                        //     likes: 0, // doc.data().likes
                        //     //lastName: doc.data().lastName
                        // }

                        doc.data().filmID.get().then((film) => {
                            recentFilm.filmGenre = film.data().filmGenre;
                            recentFilm.filmTitle = film.data().filmTitle;
                            recentFilms.push(recentFilm);
                            this.setState({ films:recentFilms})
                        })

                        
                    }
                    // console.log(film.filmTitle);
                    // console.log(`${doc.id} => ${doc.data()}`);
                });
                
            // this.setState({films});
               

                db.collection("films")
                .get().then((filmQuery) => {
                    filmQuery.forEach(doc => {
                        const film = {
                            key: doc.id,
                            createdAt: doc.data().createdAt,
                            filmTitle: doc.data().filmTitle,
                            filmGenre: doc.data().filmGenre,
                            // likes: 0, // doc.data().likes
                            //lastName: doc.data().lastName
                        }
                        films.push(film);
                    });
                        // const film = {
                        //     createdAt: doc.data().createdAt,
                        //     filmTitle: doc.data().filmTitle,
                        //     filmGenre: doc.data().filmGenre,
                        //     likes: 0, // doc.data().likes
                        //     //lastName: doc.data().lastName
                        // }
                        
                        // console.log(film.filmTitle);
                        // console.log(`${doc.id} => ${doc.data()}`);
                    });
        })
        
    }

    handleFilmLikeClicked(index) {
        const {films} = this.state;
        films[index].liked = true;

        return db.collection("userfilm").doc(films[index].id).update(
            {
                liked: true
            }
        )
        .then(() => {
             this.setState({films});
        })
    }



    formatFilm(film) {

        
        return (`${film.filmTitle}`)
        //<table>
        //{film.map(row => {
        //  <TableRow row={row} />
        //})}
        //</table>
    
    }

    render(){
        const {films} = this.state;
        // const {users} = this.state;
        //if (!auth.uid) return <Redirect to = '/login' />
        // db.collection("users").where("email", "==", "jswales2015@gmail.com")
        // .get().then((querySnapshot) => {
        //     querySnapshot.forEach((doc) => {
        //         console.log(`${doc.id} => ${doc.data()}`);
        //     });
        // });
        // db.collection("users").get().then((querySnapshot) => {
        //     querySnapshot.forEach((doc) => {
        //         console.log(`${doc.id} => ${doc.data()}`);
        //     });
        // });
        if (films) {
            return(
                <RecentFilms films={films} onFilmLikeClicked={this.handleFilmLikeClicked} />
            )
        } 
        return null;
    }
}

export default recent;

// export default recent;
// const mapStateToProps = (state) => {
//     return {
//         films: state.firestore.data.films,
//         auth: state.firebase.auth,
//     }
// }
// export default compose(
//     firestoreConnect([
//         { collection: 'films' , limit: 5 },
//     ]),
//     connect(mapStateToProps)
// )(recent);


