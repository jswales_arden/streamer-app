import React from 'react';

function SelectFilmItem(props) { 
  const film = props.film;
    
  return (
    <tr>
        <td>{film.filmTitle}</td>
        <td>{film.filmGenre}</td>
        <td>
          {<a class="waves-effect waves-light btn" onClick={props.onFilmSelectedClicked}>Select</a>}
        </td>
    </tr>
  )
}

export default SelectFilmItem;