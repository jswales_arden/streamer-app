import React, {Component} from 'react';
import firebase from '../../config/fbConfig'
import SelectFilmList from './SelectFilmList';

var db = firebase.firestore(); 

class SelectFilm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            films: []
        };
        this.handleFilmSelectedClicked = this.handleFilmSelectedClicked.bind(this);
    }

    findFilmByFilmID(films, id) {
        var i;
        for (i = 0; i < films.length; ++i) {
            if (films[i].filmID === id) {
                return films[i];
            }
        }
        return null;
    }
  
    componentWillMount() {
        var userID = firebase.auth().currentUser.uid;
        const films = [];
        const recentFilms = [];
        const availableFilms = [];

        db.collection("films")
            .get().then((filmQuery) => {
                filmQuery.forEach(doc => {
                    const film = {
                        key: doc.id,
                        createdAt: doc.data().createdAt,
                        filmTitle: doc.data().filmTitle,
                        filmGenre: doc.data().filmGenre
                    }
                    films.push(film);
                });

                db.collection("userfilm")
                    .get().then((querySnapshot) => {
                        querySnapshot.forEach((doc) => {
                            if (doc.data().userID.id === userID) {
                                const recentFilm = {
                                    id: doc.id,
                                    filmID: doc.data().filmID.id,
                                    liked: doc.data().liked
                                }
                                recentFilms.push(recentFilm);   
                            }
                        });

                        var i;
                        for (i = 0; i < films.length; ++i) {
                            const film = this.findFilmByFilmID(recentFilms, films[i].key)
                            if (film == null) {
                                films[i].selected = false;
                                availableFilms.push(films[i])
                                
                            }
                        }
                        
                        this.setState({films: availableFilms});
                })
            })
    }

    handleFilmSelectedClicked(index) {
        var userID = firebase.auth().currentUser.uid;
        const {films} = this.state;
        films[index].liked = true;

        return db.collection("userfilm").doc().set(
            {
                userID: db.doc('users/' + userID),
                filmID: db.doc('films/' + films[index].key),
                liked: false
            }
        )
        .then(() => {
             this.setState({films});
        })
    }

    render(){
        const {films} = this.state;
       
        return(
            <SelectFilmList films={films} onFilmSelectedClicked={this.handleFilmSelectedClicked} />
        )
    }
}
export default SelectFilm;

