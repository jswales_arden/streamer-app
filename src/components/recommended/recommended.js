//imports
import React, {Component} from 'react';


import FilmList from '../films/filmList';
import { connect } from 'react-redux'
import { firestoreConnect } from  'react-redux-firebase'
import { compose } from 'redux'
import {Redirect} from 'react-router-dom'
import moment from 'moment';

class recommended extends Component {
    render(){
        const {films} = this.props;
        //if (!auth.uid) return <Redirect to = '/login' />

        return(
                

                <div className="container">
                    
                    <h5 className="grey-text-text-darken-3">Recommended for you</h5>
                    <p>Based on your recent activity, we recommend...</p>
                            <FilmList  films = {films}/>

                        
                        {/* //{props.profile.initials} */}

                    </div>
 

        )
    }
}

const mapStateToProps = (state) => {
    return {
        films: state.firestore.data.films,
        auth: state.firebase.auth,
    }
}
export default compose(
    firestoreConnect([
        { collection: 'films' , limit: 5 },
    ]),
    connect(mapStateToProps)
)(recommended);


