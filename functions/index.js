const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

const createNotification = ((notification => {
    return admin.firestore().collection('notifications')
        .add(notification)
        .then(doc => console.log('notification added', doc));
}));


exports.filmAdded = functions.firestore
    .document('films/{filmID}')
    .onCreate( doc => {
        const film = doc.data();
        const notification = {
            content: `'Watched a new film: ${film.filmTitle}`,
            user: `${film.userFirstName} ${film.userLastName}`,
            likes: `${film.likes}`,
            time: admin.firestore.FieldValue.serverTimestamp()
        }
        return createNotification(notification);

});

exports.userJoined = functions.auth.user()
    .onCreate(user => {
        return admin.firestore.collection('users')
            .doc(user.uid).get().then(doc => {
                const newUser = doc.data();
                const notification = {
                    content: 'Joined Streamer',
                    user: `${newUser.userFirstName} ${newUser.userLastName}`,
                    time: admin.firestore.FieldValue.serverTimestamp()
                };

                return createNotification(notification);
            });
    });
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
